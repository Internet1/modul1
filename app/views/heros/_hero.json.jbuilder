json.extract! hero, :id, :nickname, :rank, :charisma, :wisdom, :created_at, :updated_at
json.url hero_url(hero, format: :json)
