Rails.application.routes.draw do
  resources :heros

  get 'heros/index'
  root "heros#index"
end
